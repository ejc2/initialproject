import { ref, set } from "firebase/database"
import { db } from "../config/firebase-config"


export const registerMember = (firstName, lastName, sex, email, address, cprNr) => {
    set(ref(db, 'members/' + cprNr), {
        firstName,
        lastName,
        sex,
        email,
        address,
        cprNr,
        registered: `${new Date()}`
    })
}
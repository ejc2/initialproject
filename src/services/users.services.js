import { get, ref, set } from "firebase/database";
import { db } from "../config/firebase-config.js";


export const createUserDB = (uid, email) => {
    return set(ref(db, `users/${uid}`), { email, admin:false })
  };

  export const getUserByUid = async (uid) => {
    try {
      const snapshot = await get(ref(db, `users/${uid}`));
      if (snapshot.exists()) {
        console.log(snapshot.val(), '<============userData')
        const userData = snapshot.val()
         return userData;
      }
  
    } catch (error) {
      console.error(error);
    }
  };
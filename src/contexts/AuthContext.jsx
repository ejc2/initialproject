import {  useEffect, useState } from "react";
import { auth } from "../config/firebase-config";
import { getUserByUid } from "../services/users.services";
import { AuthContext } from "../services/auth.services";




export function AuthProvider({ children }) {
    const [currentUser, setCurrentUser] = useState(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        
        const unsubscribe = auth.onAuthStateChanged(async (user) => {
            if (user) {
                setCurrentUser(user);
                const fetchedData = await getUserByUid(user.uid);
                setCurrentUser(fetchedData)
            } else {
                setCurrentUser(null);
            }
            setLoading(false);
        });

        return () => unsubscribe();
    }, []);

    const value = {
        currentUser,
    };

    return (
        <AuthContext.Provider value={value}>
            {!loading && children}
        </AuthContext.Provider>
    );
}

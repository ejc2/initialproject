import { useState } from 'react';
import { Box, Button, FormControl, FormLabel, Input, Heading, Stack, Text, Link } from '@chakra-ui/react';
import { useNavigate } from 'react-router-dom';
import { registerNewUser, useAuth } from '../../services/auth.services';


const SignUpForm = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const navigate = useNavigate()
  const { currentUser } = useAuth()

  const handleSignUp = async (e) => {
    e.preventDefault()
    //validations
    try {
      if (password.length < 7) {
        alert('Password must be at least 7 symbols')
      }
      if (password !== confirmPassword) {
        alert('Passwords don\'t match')
      }
       registerNewUser(email, password)
       navigate('/')
    } catch (e) {
      alert(e.message)
    }
  };

  return (
    <Box maxW="md" mx="auto" mt={8} p={4}>
      {currentUser && currentUser.email}
      <Heading size="lg" textAlign="center" mb={6}>
        Sign Up
      </Heading>
      <Stack spacing={4}>
        <FormControl>
          <FormLabel>Email</FormLabel>
          <Input
            type="email"
            placeholder="Enter your email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value)
              console.log(email)
            }}
          />
        </FormControl>
        <FormControl>
          <FormLabel>Password</FormLabel>
          <Input
            type="password"
            placeholder="Enter your password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </FormControl>
        <FormControl>
          <FormLabel>Confirm Password</FormLabel>
          <Input
            type="password"
            placeholder="Confirm your password"
            value={confirmPassword}
            onChange={(e) => setConfirmPassword(e.target.value)}
          />
        </FormControl>
        <Button colorScheme="blue" onClick={handleSignUp}>
          Sign Up
        </Button>
        <Text textAlign="center">
          Already have an account?{' '}
          <Link color="blue.500"
            href="#"
            onClick={() => navigate('/sign-in')}>
            Log in HERE!
          </Link>          
        </Text>
      </Stack>
    </Box>
  );
};

export default SignUpForm;

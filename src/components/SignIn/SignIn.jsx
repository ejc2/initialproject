import { useState } from 'react';
import { Box, Button, FormControl, FormLabel, Input, Heading, Stack, Text, Link } from '@chakra-ui/react';
import { useNavigate } from 'react-router-dom';
import { signIn, useAuth } from '../../services/auth.services';

const SignInForm = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const { currentUser } = useAuth()

    const navigate = useNavigate()
    const handleSignIn = (e) => {
        e.preventDefault()
        signIn(email, password)
        navigate('/')
    };

    
    return (
        <Box maxW="md" mx="auto" mt={8} p={4}>
            <Heading size="lg" textAlign="center" mb={6}>
                Sign In
            </Heading>
            {currentUser && currentUser.email}
            <Stack spacing={4}>
                <FormControl>
                    <FormLabel>Email</FormLabel>
                    <Input
                        type="email"
                        placeholder="Enter your email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                    />
                </FormControl>
                <FormControl>
                    <FormLabel>Password</FormLabel>
                    <Input
                        type="password"
                        placeholder="Enter your password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </FormControl>
                <Button colorScheme="blue" onClick={handleSignIn}>
                    Sign In
                </Button>
                <Text textAlign="center">
                    Don't have an account?
                    <Link
                        //   onClick={handleClickSignUp}
                        onClick={() => navigate('/sign-up')}
                        color="blue.500" href="#">
                        Sign up HERE!
                    </Link>
                </Text>
            </Stack>
        </Box >
    );
};

export default SignInForm;

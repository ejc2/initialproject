
const StickySidebar = ({children}) => {

  return (
    <div className="left-sidebar">
      
      {children}
      
    </div>
  );
};

export default StickySidebar;
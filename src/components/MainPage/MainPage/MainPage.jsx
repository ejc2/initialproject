import { VStack } from "@chakra-ui/react";
import Desktop from "../Desktop.jsx";
import StickyHeader from "../StickyHeader.jsx";
import StickySidebar from "../StickySidebar.jsx";
import "./MainPage.css";
import SidebarButton from "../SidebarButton.jsx";
import { registerMember } from "../../../services/members.services.js";
import { useState } from "react";

const MainPage = () => {
  const [name, setName] = useState("");

  const ticketAction = (argument) => {
    //firebase code here
    // registerMember(
    //   "Pesho",
    //   "Peshov",
    //   "Man",
    //   "pesho@abv.bg",
    //   "Gammel kongevej 4",
    //   "1207873549"
    // );
    setName(argument);
  };
  return (
    <div className="main-page">
      <StickyHeader />
      <div className="content-wrapper">
        <StickySidebar>
          <VStack>
            <SidebarButton
              label={"+"}
              action={() => {
                registerMember(
                  "Pesho",
                  "Peshov",
                  "Man",
                  "pesho@abv.bg",
                  "Gammel kongevej 4",
                  "1207873549"
                );
              }}
            />
            <SidebarButton label={"Ticket"} action={ticketAction} />
            <SidebarButton label={"Card"} />
            <SidebarButton label={"Products"} />
          </VStack>
        </StickySidebar>
        <Desktop propName={name} />
      </div>
    </div>
  );
};

export default MainPage;

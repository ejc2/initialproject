import { Button } from "@chakra-ui/react";

const SidebarButton = ({ label, action }) => {
  return (
    <Button
      onClick={() => {
        action("Pesho");
      }}
      bg="#355C7D"
      color="white"
      borderRadius="full"
      px={6}
      py={3}
      fontWeight="bold"
      _hover={{
        transform: "translateY(-2px)",
        boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
      }}
    >
      {label}
    </Button>
  );
};

export default SidebarButton;

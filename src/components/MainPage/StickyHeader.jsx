import { Box, Flex, Image, Spacer } from '@chakra-ui/react';
import UserInfo from '../UserInfo/UserInfo';

const StickyHeader = () => {
    return (
        <div className="header">
           <Flex 
        //    bg="gray.700" 
           p={4} 
           alignItems="center">
      {/* Other header content on the left */}
      <Box maxHeight={100} maxWidth={100}>
        {/* Your logo or other content */}
        <Image src='https://png.pngitem.com/pimgs/s/691-6915922_barbell-png-transparent-png.png' onClick={()=>{console.log('logo clicked')}}/>
      </Box>

      {/* Spacer to push UserInfo to the right */}
      <Spacer/>
      

      {/* UserInfo component on the right */}
      <Box>
        <UserInfo />
      </Box>
    </Flex>
        </div>
      );
};

export default StickyHeader;
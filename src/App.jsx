import SignIn from "./components/SignIn/SignIn"
import SignUp from "./components/SignUp/SignUp"
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import { AuthProvider } from "./contexts/AuthContext"
import MainPage from "./components/MainPage/MainPage/MainPage.jsx"





function App() {

  return (
    <>
      <AuthProvider>
        <Router>
          <Routes>

            <Route path="/" Component={MainPage} />
            <Route path="/sign-in" Component={SignIn} />
            <Route path="/sign-up" Component={SignUp} />

          </Routes>
        </Router>
      </AuthProvider>
    </>
  )
}

export default App
